1) Реализовать abstract class RationalFraction<T>
   1) У него должно быть 2 аттрибута:
      1) T numerator
      2) T denominator
   2) Он должен имплементить интерфейсы Addition<T>, Subtraction<T>, Multiplication<T>, Division<T>
      1) У каждого из этих интерфейсов есть лишь один метод(Например):
         1) interface Addition<T> 
            1) T add(T o1, T o2);
2) Реализовать class IntegerRationalFraction extends RationalFraction<Integer>
   1) следовательно реализовав все метода интерфесов