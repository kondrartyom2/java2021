package CW.Lesson17dop;

import CW.Lesson17.Creature;

public abstract class RationalFraction<T extends Number> implements Math<T> {
    private T numerator;
    private T denominator;

    public RationalFraction(T numerator, T denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public T getNumerator() {
        return numerator;
    }

    public T getDenominator() {
        return denominator;
    }
}
