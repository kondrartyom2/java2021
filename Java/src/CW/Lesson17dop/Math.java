package CW.Lesson17dop;

public interface Math<T extends Number> {
    T add(T num1, T num2);
    T sub(T o);
    T div(T o);
    T mul(T o);
}
