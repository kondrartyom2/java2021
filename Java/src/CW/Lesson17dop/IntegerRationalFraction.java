package CW.Lesson17dop;

public class IntegerRationalFraction extends RationalFraction<Integer> {
    public IntegerRationalFraction(Integer numerator, Integer denominator) {
        super(numerator, denominator);
    }

    @Override
    public Integer add(Integer num1, Integer num2) {
        return null;
    }

    @Override
    public Integer sub(Integer o) {
        return null;
    }

    @Override
    public Integer div(Integer o) {
        return null;
    }

    @Override
    public Integer mul(Integer o) {
        return null;
    }
}
