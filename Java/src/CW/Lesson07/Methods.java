package CW.Lesson07;

import java.util.Scanner;

public class Methods {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введи длину массива");
        int[] arr = new int[scanner.nextInt()];
        fillTheArray(arr);
        printMyArray(arr);
        int x = 3;
        inc(x);
        System.out.println(x);
    }

    public static int inc(int x) {
        x++;
        return x;
    }

    public static void printMyArray(int[] arr) {
        for (int elem : arr) {
            System.out.print(elem + ", ");
        }
        System.out.println();
    }

    public static void fillTheArray(int[] arr) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Введите число");
            arr[i] = scanner.nextInt();
        }
    }
}
