package CW.Lesson16;

public class Main {
    public static void main(String[] args) {
        RationalFraction rationalFraction = new RationalFraction(1, 2);
        RationalFraction rationalFraction1 = new RationalFraction(2, 4);
        RationalFraction rationalFraction2 = rationalFraction.add(rationalFraction1);
        System.out.println(rationalFraction2);
        System.out.println(rationalFraction);
        System.out.println(rationalFraction.equals(rationalFraction1));
        System.out.println(rationalFraction1);
    }
}
