package CW.Lesson16;

public class RationalFraction {
    private int numerator;
    private int denominator;

    public RationalFraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public RationalFraction add(RationalFraction rationalFraction) {
        int denominator = getGeneralDenominator(this.denominator, rationalFraction.denominator);
        int numerator = getMultipliedNumerator(this.numerator, rationalFraction.denominator) +
                getMultipliedNumerator(rationalFraction.numerator, this.denominator);
        RationalFraction answer = new RationalFraction(numerator, denominator);
        answer.reduce();
        return answer;
    }

    public RationalFraction sub(RationalFraction rationalFraction) {
        int denominator = getGeneralDenominator(this.denominator, rationalFraction.denominator);
        int numerator = getMultipliedNumerator(this.numerator, rationalFraction.denominator) -
                getMultipliedNumerator(rationalFraction.numerator, this.denominator);
        RationalFraction answer = new RationalFraction(numerator, denominator);
        answer.reduce();
        return answer;
    }

    public void add2(RationalFraction rationalFraction) {
        int denominator = getGeneralDenominator(this.denominator, rationalFraction.denominator);
        this.numerator = getMultipliedNumerator(this.numerator, rationalFraction.denominator) +
                getMultipliedNumerator(rationalFraction.numerator, this.denominator);
        this.denominator = denominator;
        this.reduce();
    }

    public void sub2(RationalFraction rationalFraction) {
        this.denominator = getGeneralDenominator(this.denominator, rationalFraction.denominator);
        this.numerator = getMultipliedNumerator(this.numerator, rationalFraction.denominator) -
                getMultipliedNumerator(rationalFraction.numerator, this.denominator);
        this.reduce();
    }

    public RationalFraction mul(RationalFraction rationalFraction) {
        RationalFraction answer = new RationalFraction(this.numerator * rationalFraction.numerator,
                this.denominator * rationalFraction.denominator);
        answer.reduce();
        return answer;
    }

    public RationalFraction div(RationalFraction rationalFraction) {
        RationalFraction answer = new RationalFraction(this.denominator * rationalFraction.numerator,
                this.denominator * rationalFraction.numerator);
        answer.reduce();
        return answer;
    }

    public void mul2(RationalFraction rationalFraction) {
        this.numerator = this.numerator * rationalFraction.numerator;
        this.denominator = this.denominator * rationalFraction.denominator;
    }

    public void div2(RationalFraction rationalFraction) {
        this.numerator = this.numerator * rationalFraction.denominator;
        this.denominator = this.denominator * rationalFraction.numerator;
    }

    @Override
    public String toString() {
        return String.format("%d/%d", numerator, denominator);
    }

    public void reduce() {
        int limit = Math.min(getDenominator(), getNumerator());
        for (int i = limit; i > 0; i--) {
            if (getNumerator() % i == 0 && getDenominator() % i == 0) {
                this.numerator = this.numerator / i;
                this.denominator = this.denominator / i;
            }
        }
    }

    public boolean equals(RationalFraction rationalFraction) {
        if (this == rationalFraction) {
            return true;
        } else if (this.numerator == rationalFraction.numerator && this.denominator == rationalFraction.denominator) {
            return true;
        } else {
            RationalFraction rationalFraction1 = new RationalFraction(this.numerator, this.denominator);
            RationalFraction rationalFraction2 = new RationalFraction(rationalFraction.numerator, rationalFraction.denominator);
            rationalFraction1.reduce();
            rationalFraction2.reduce();
            if (rationalFraction1.numerator == rationalFraction2.numerator && rationalFraction1.denominator == rationalFraction2.denominator) {
                return true;
            } else {
                return false;
            }
        }
    }

    private int getGeneralDenominator(int denominator, int otherDenominator) {
        return denominator * otherDenominator;
    }

    private int getMultipliedNumerator(int numerator, int otherDenominator) {
        return numerator * otherDenominator;
    }
}
