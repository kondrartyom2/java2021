package CW.Lesson05;

import java.util.Scanner;

public class ArraysAndForEach {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите кол-во чисел, которые вы хотите ввести - ");
        int[] array = new int[scanner.nextInt()];
        System.out.println("Длинна массива - " + array.length);
        for (int i = 0; i < array.length; i++) {
            System.out.print("Введите число - ");
            array[i] = scanner.nextInt();
        }

        for (int element : array) {
            System.out.println(element);
        }
    }
}
