package CW.Lesson11;

import java.util.Scanner;

public class HWHelp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число - ");
        int num = scanner.nextInt();
        int spaces = num - 1;
        for(int i = 0; i < num; i++) {
            for(int j = 0; j < spaces; j++) {
                System.out.print(" ");
            }
            for(int j = 0; j < num; j++) {
                System.out.print("0 ");
            }
            System.out.println();
            spaces--;
        }
    }
}