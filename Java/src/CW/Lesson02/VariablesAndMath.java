package CW.Lesson02;

public class VariablesAndMath {

    public static void main(String[] args) {
        byte k = 2;
        short t = 25;
        int r = 100;
        long l = 150L;
        float f = 3.5f;
        double d = 3.55;
        char c = 'd';

        int sum = k + t;
        int raz = k - t;
        int mul = k * t;
        int del = r / t;

        //r++
        r = r + 1;

        //r--
        r = r - 1;

        //r = r / t
        r /= t;

        //r = r * t
        r *= t;

        //r = r - t
        r -= t;

        //r = r + t
        r += t;

        //остаток от деления
        System.out.println(t % 2);

        System.out.println((t % 2) == 0);

        boolean dn = true;
        dn = false;

        //больше
        System.out.println(k > 2);
        //меньше
        System.out.println(k < 2);
        //больше или равно
        System.out.println(k >= 2);
        //меньше или равно
        System.out.println(k <= 2);
        //равно
        System.out.println(k == 2);
        //не равно
        System.out.println(k != 2);
        //отрицание
        System.out.println(!dn);
        //эквивалентно ==
        System.out.println(!(k != 2));

        System.out.println((k > 2) & (k < 5));
        System.out.println((k > 2) | (k < 5));
        System.out.println((k > 2) && (k < 5));
        System.out.println((k > 2) || (k < 5));


    }
}
