package CW.Lesson04;

public class CheckBrains {
    public static void main(String[] args) {
        int x = 10;
        if (x > 5) {
            x /= 2;
        } else if (x < 0) {
            System.out.println("error");
        } else {
            x = 0;
        }

        int y;
        if (x > 5) {
            y = 2;
        } else if (x < 0) {
            y = x * x;
        } else {
            y = 0;
        }
        y = (x > 5) ? 2 : ((x < 0) ? x * x : 0);

    }
}
