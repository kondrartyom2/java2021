package CW.Lesson08;

import java.util.Scanner;

public class LessonEight {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = getArray();
        int intNum = scanner.nextInt();
        double doubleNum = scanner.nextDouble();
        intNum = elevate(intNum);
        doubleNum = elevate(doubleNum);
        elevate(array);
        System.out.println(intNum);
        System.out.println(doubleNum);
        System.out.println(toString(array));

    }

    public static void elevate(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] *= array[i];
        }
    }

    public static double elevate(double doubleNum) {
        return doubleNum * doubleNum;
    }

    public static int elevate(int intNum) {
        return intNum * intNum;
    }

    public static String toString(int[] array) {
        StringBuilder answer = new StringBuilder("[");
        for (int elem : array) {
            answer.append(elem);
            answer.append(", ");
        }
        answer.append("]");
        return answer.toString();
    }

    public static int[] getArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите кол-во элементов - ");
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        for (int i = 0; i < array.length; i++) {
            System.out.print("Введите элемент - ");
            array[i] = scanner.nextInt();
        }
        return array;
    }
}
