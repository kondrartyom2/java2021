package CW.Lesson03;

import java.util.Scanner;

public class IfWithElse {
    public static void main(String[] args) {
        System.out.print("Введите число от 1 до 5 - ");
        Scanner s = new Scanner(System.in);
        int num = s.nextInt();
//        if (num > 0) {
//            System.out.println("Твое число больше 0");
//        } else if (num < 0) {
//            System.out.println("Твое число меньше 0");
//        } else {
//            System.out.println("Твое число 0");
//        }
//        if (num == 1) {
//            System.out.println("One");
//        } else if (num == 2) {
//            System.out.println("Two");
//        } else if (num == 3) {
//            System.out.println("Three");
//        } else if (num == 4) {
//            System.out.println("Four");
//        } else if (num == 5) {
//            System.out.println("Five");
//        } else {
//            System.out.println("Error");
//        }

//        switch (num) {
//            case 1:
//                System.out.println("One");
//                break;
//            case 2:
//                System.out.println("Two");
//                break;
//            case 3:
//                System.out.println("Three");
//                break;
//            case 4:
//                System.out.println("Four");
//                break;
//            case 5:
//                System.out.println("Five");
//                break;
//            default:
//                System.out.println("Error");
//        }
        System.out.println(((num == 3) || (num == 5)) ? "Не степень двойки" : "Степень");
    }
}
