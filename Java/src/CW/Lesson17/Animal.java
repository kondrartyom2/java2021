package CW.Lesson17;

public abstract class Animal extends Creature implements Live {

    private String name;
    private String species;

    public Animal(int age, int weight, String name, String species) {
        super(age, weight);
        this.name = name;
        this.species = species;
    }

    @Override
    public boolean eat(String product) {
        return product.equals("еда");
    }

    @Override
    public boolean drink(String product) {
        return product.equals("вода");
    }
}
