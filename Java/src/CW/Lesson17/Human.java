package CW.Lesson17;

public class Human extends Creature implements Speaking, Thinking {

    public Human(int age, int weight) {
        super(age, weight);
    }

    @Override
    public boolean eat(String product) {
        return product.equals("блюдо");
    }

    @Override
    public boolean drink(String product) {
        return product.equals("стакан");
    }

    @Override
    public String speak() {
        return "говорю";
    }

    @Override
    public void think() {
        System.out.println("Думаю");
    }
}
