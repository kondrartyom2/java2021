package CW.Lesson17;

public class Dog extends Animal implements Speaking {
    private String gav;
    public Dog(int age, int weight, String name, String species, String gav) {
        super(age, weight, name, species);
        this.gav = gav;
    }

    public String getGav() {
        return gav;
    }

    public void setGav(String gav) {
        this.gav = gav;
    }

    @Override
    public String speak() {
        return this.getGav();
    }
}
