package CW.Lesson17;

public abstract class Creature implements Listening {
    private int age;
    private int weight;

    public Creature(int age, int weight) {
        this.age = age;
        this.weight = weight;
    }

    @Override
    public void listen(String phrase) {
        System.out.println("Слушаю");
    }

    public abstract boolean eat(String product);

    public abstract boolean drink(String product);

    public void grownOld() {
        this.age++;
    }

    public int getAge() {
        return age;
    }

    public int getWeight() {
        return weight;
    }
}
