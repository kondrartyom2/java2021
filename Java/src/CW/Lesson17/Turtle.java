package CW.Lesson17;

public class Turtle extends Animal {
    public Turtle(int age, int weight, String name, String species) {
        super(age, weight, name, species);
    }

    @Override
    public boolean eat(String product) {
        return product.equals("вода");
    }
}
