package CW.Lesson17;

public class Cat extends Animal implements Speaking {
    private String may;

    public Cat(int age, int weight, String name, String species, String may) {
        super(age, weight, name, species);
        this.may = may;
    }

    public String getMay() {
        return may;
    }

    public void setMay(String may) {
        this.may = may;
    }

    @Override
    public String speak() {
        return this.getMay();
    }
}
