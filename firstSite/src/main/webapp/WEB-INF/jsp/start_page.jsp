<%--
  Created by IntelliJ IDEA.
  User: Artem Work
  Date: 24.05.2021
  Time: 1:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>It park</title>
    <link rel="stylesheet" href="../../static/css/start_style.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/buttons.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/blocks.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/navbar.css" type="text/css"/>
    <link rel="stylesheet" href="../../static/css/carousel_home.css" type="text/css"/>
    <link rel="icon" href="../../static/images/logo.jpg" type="image/x-icon"/>
    <link rel="shortcut icon" href="../../static/images/logo.jpg" type="image/x-icon"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Goldman:wght@400;700&display=swap" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.js"></script>
    <script src="../../static/js/home.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.js"></script>
</head>
<body style="background-color: rgba(0, 0, 0, 0.7);">
<header>
    <div class="nav transparent navbar-inverse navbar-fixed-top">
        <nav class="navbar-inner navbar-expand-md sticky">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">
                        <span>It Park</span>
                    </a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/signin" >
                            <span class="glyphicon glyphicon-user" style="color: white"></span>
                            <span style="color: white; font-weight: bolder"> Вход</span>
                        </a></li>
                        <li><a href="/signup">
                            <span class="glyphicon glyphicon-log-in" style="color: white"></span>
                            <span style="color: white; font-weight: bolder"> Регистрация</span>
                        </a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
</body>
</html>