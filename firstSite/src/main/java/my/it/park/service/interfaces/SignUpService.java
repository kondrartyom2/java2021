package my.it.park.service.interfaces;

import my.it.park.dto.UserDto;

public interface SignUpService {
    Boolean signUp(UserDto userDto);
}
