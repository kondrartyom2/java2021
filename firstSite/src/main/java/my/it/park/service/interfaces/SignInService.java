package my.it.park.service.interfaces;

import my.it.park.dto.UserDto;

public interface SignInService {
    UserDto signIn(UserDto userDto);
}
