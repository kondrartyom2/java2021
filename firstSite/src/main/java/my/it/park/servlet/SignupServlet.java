package my.it.park.servlet;

import my.it.park.dto.UserDto;
import my.it.park.service.interfaces.SignInService;
import my.it.park.service.interfaces.SignUpService;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/signup")
public class SignupServlet extends HttpServlet {

    private ApplicationContext applicationContext;

    private ServletContext contextListener;

    private SignUpService signUpService;

    private SignInService signInService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.contextListener = config.getServletContext();
        this.applicationContext = (ApplicationContext) contextListener.getAttribute("applicationContext");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/signup_page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Boolean signUp = signUpService.signUp(
                UserDto.builder()
                        .firstName(request.getParameter("firstName"))
                        .email(request.getParameter("email"))
                        .hashPassword(request.getParameter("password"))
                        .build()
        );

        UserDto userDto = signInService.signIn(
                UserDto.builder()
                        .email(request.getParameter("email"))
                        .hashPassword(request.getParameter("password"))
                        .build()
        );

        if (signUp) {
            HttpSession session = request.getSession(true);
            session.setAttribute("userDto", userDto);
            response.sendRedirect("/");
        } else response.sendRedirect("/signup");
    }
}
