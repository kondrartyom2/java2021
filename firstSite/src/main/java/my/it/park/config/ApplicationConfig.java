package my.it.park.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:db.properties")
@ComponentScan(basePackages = "my.it.park")
public class ApplicationConfig {
}
